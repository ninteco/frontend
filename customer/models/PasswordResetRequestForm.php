<?php
namespace customer\models;

use common\models\Customer;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $customer_email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['customer_email', 'filter', 'filter' => 'trim'],
            ['customer_email', 'required'],
            ['customer_email', 'email'],
            ['customer_email', 'exist',
                'targetClass' => '\common\models\Customer',
                'filter' => ['status' => Customer::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user Customer */
        $user = Customer::findOne([
            'status' => Customer::STATUS_ACTIVE,
            'customer_email' => $this->customer_email,
        ]);

        if ($user) {
            if (!Customer::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }

            if ($user->save()) {
                return \Yii::$app->mailer->compose(['html' => 'passwordResetTokenCustomer-html', 'text' => 'passwordResetTokenCustomer-text'], ['user' => $user])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                    ->setTo($this->customer_email)
                    ->setSubject('Password reset for ' . \Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }
}
