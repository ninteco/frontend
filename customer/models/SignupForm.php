<?php
namespace customer\models;

use common\models\Customer;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $customer_username;
    public $customer_email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['customer_username', 'filter', 'filter' => 'trim'],
            ['customer_username', 'required'],
            ['customer_username', 'unique', 'targetClass' => '\common\models\Customer', 'message' => 'This username has already been taken.'],
            ['customer_username', 'string', 'min' => 2, 'max' => 255],

            ['customer_email', 'filter', 'filter' => 'trim'],
            ['customer_email', 'required'],
            ['customer_email', 'email'],
            ['customer_email', 'string', 'max' => 255],
            ['customer_email', 'unique', 'targetClass' => '\common\models\Customer', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new Customer();
            $user->customer_username = $this->customer_username;
            $user->customer_email = $this->customer_email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
