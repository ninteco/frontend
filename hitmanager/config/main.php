<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-hitmanager',
	'name'=>'Hit Manager App',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'hitmanager\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
	
		'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			]
        ],		       
        'user' => [
            'identityClass' => 'common\models\HitManager',
            'enableAutoLogin' => true,
			'identityCookie' => [
                'name' => '_hitmanagerUser', // unique for hitmanager
            ]
        ],		
		'session' => [
            'name' => 'PHPFRONTSESSID',
            'savePath' => sys_get_temp_dir(),
        ],		
		'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'HU38RzsI2BpFDu8f6q4pq90LZaYUZLnB',
            'csrfParam' => '_hitmanagerCSRF',
        ],		
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
